// Eliminar de Local Storage
localStorage.clear();
//creamos la base de datos de comentarios

var dbcomentarios = localStorage.getItem("dbcomentarios"); //Obtener datos de localStorage
arreglo = JSON.parse(dbcomentarios); // Covertir a objeto
if (arreglo === null)
  // Si no existe, creamos un array vacio.
  arreglo = [];
function Mensaje(t) {
  switch (t) {
    case 1: //
      $(".mensaje-alerta").append("Se agrego el comentario con exito");
      break;
    case 2: //
      $(".mensaje-alerta").append("No se agrego el comentario con éxito");
      break;
    default:
  }
}

function Agregar() {
  // Seleccionamos los datos de los inputs de formulario
  var datos_cliente = JSON.stringify({
    Nombre: $("#nombre").val(),
    Correo: $("#correo").val(),
    Comentario: $("#comentario").val(),
  });

  arreglo.push(datos_cliente); // Guardar datos en el array definido globalmente
  localStorage.setItem("dbcomentarios", JSON.stringify(arreglo));
  Listar();
  return Mensaje(1);
}
function Listar() {
  $("#tbl-lista").html(
    "<thead>" +
      "<tr>" +
      "<th> No </th>" +
      "<th> Nombre </th>" +
      "<th> Correo </th>" +
      "<th> Comentario </th>" +
      "</tr>" +
      "</thead>" +
      "<tbody>" +
      "</tbody>"
  );
  var indice = 0;
  for (var i in arreglo) {
    var d = JSON.parse(arreglo[i]);

    $("#tbl-lista").append(
      "<tr>" +
        "<td>" +
        (indice + 1) +
        "</td>" +
        "<td>" +
        d.Nombre +
        "</td>" +
        "<td>" +
        d.Correo +
        "</td>" +
        "<td>" +
        d.Comentario +
        "</td>" +
        "</tr>"
    );
    indice++;
  }
}
